# Gestion de empleados.

## Configuracion

### Base de datos

Motor: Postgresql

DB: prueba

Schemas: pkg/persistencia/postgresql/sql/schema.sql

Insert: pkg/persistencia/postgresql/sql/querys.sql


### API

Consumo Api: Cidenet.postman_collection.json

#### Correr Manual
DNS Ejemplo DB: 'host=127.0.0.1 user=postgres password=postgres dbname=prueba port=5432 sslmode=disable TimeZone=America/Bogota'

Env: API_DB_DNS

Run Api: export API_DB_DNS='{DNS Ejemplo DB}'  go run cmd/server/main.go    

#### Correr con Docker

Build: docker build -t prueba:v1 .
Run: docker run -e API_DB_DNS='{DNS Ejemplo DB}' -p 8180:8180 prueba:v1

## Funciones requeridas.

1. Registro de empleados.
1. Consulta de empleados.
1. Editar empleado.
1. Eliminar empleado.

## Api

Especificacion tecnica del API. 
<hr>
 
**Arquitectura: Rest**

**Base: /gestion/api/v1**

**Response: application/json**

**Metodos http:**

Accion: **Registro, Editar y Borrar Empleados:**

Grupo: **/registro**

| Metodo | EndPoint | Body | Response | Doc |
| ------ | -------- | ---- | -------- | --- |
| POST   | /nuevo | json  | json | crea un nuevo empleado |
| DELETED | /borrar/:id |  | json | eliminar un empleado |
| PUT    | /editar/:id | json  | json | edita un empleado |

Accion: **Consulta Empleados:**

Grupo: **/consulta**

| Metodo | EndPoint | Body | Response | Doc |
| ------ | -------- | ---- | -------- | --- |
| GET    | /todo|      | json | listado de todos los empleados |
| GET    | /por_id/:id | | json | lista la informacion de un empleado |

Accion: **Listados:**

Grupo: **/listas**

| Metodo | EndPoint | Body | Response | Doc |
| ------ | -------- | ---- | -------- | --- |
| GET   | /empleados |      | json | listar empleados |
| GET   | /pais |      | json | listar pais |
| GET   | /tipo_identificacion |      | json | listar identificacion |
| GET   | /areas |      | json | listar areas |



Tablas:

**Empleado**
| Campo | Tamaño | Tipo | LLave | Restriccion | 
| ----- | ------ | ---- | ----- | ----------- |
| ID    |        | UUID | PK    | UNIQUE      |
| secuencia | 1000 | INT |      |             |
| primer_apellido | 20   | TEXT | |  NOT NULL |
| segundo_apellido | 20  | TEXT | |  NOT NULL |
| primer_nombre    | 20  | TEXT | |  NOT NULL |
| otros_nombre     | 50  | TEXT | |  NOT NULL |
| pais_empleo      | UUID | UUID | FK_Pais | NOT NULL |
| tipo_identificacion | UUID | UUID | FK_Tipo_Identificacion | NOT NULL |
| numero_identificacion | 20 | INT | | UNIQUE, NOT NULL |
| correo | 300 | TEXT | | UNIQUE NOT NULL |
| fecha_ingreso | FECHA | FECHA | | NOT NULL |
| area | UUID | UUID | FK_Areas | NOT NULL |
| estado | BOOL | BOOL |  | NOT NULL, DEFAULT TRUE |
| fecha_registro | FECHA | FECHA | | NOT NULL |
| fecha_edicion | FECHA | FECHA | | NOT NULL |

**Pais**
| Campo | Tamaño | Tipo | LLave | Restriccion | 
| ----- | ------ | ---- | ----- | ----------- |
| ID    |        | UUID | PK    | UNIQUE      |
| secuencia | 1000 | INT |      |             |
| nombre | 20   | TEXT | |  NOT NULL |

**Tipo Identificacion**
| Campo | Tamaño | Tipo | LLave | Restriccion | 
| ----- | ------ | ---- | ----- | ----------- |
| ID    |        | UUID | PK    | UNIQUE      |
| secuencia | 1000 | INT |      |             |
| nombre | 50   | TEXT | |  NOT NULL |

**Areas**
| Campo | Tamaño | Tipo | LLave | Restriccion | 
| ----- | ------ | ---- | ----- | ----------- |
| ID    |        | UUID | PK    | UNIQUE      |
| secuencia | 1000 | INT |      |             |
| nombre | 50   | TEXT | |  NOT NULL |



