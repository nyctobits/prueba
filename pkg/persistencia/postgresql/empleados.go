package postgresql

import (
	"context"
	"database/sql"

	"gestion.cidenet.com/pkg/adaptadores/interfaces"
	"gestion.cidenet.com/pkg/entidades"
)

type EmpleadosModel struct {
	CaseUse interfaces.IUseCase
	DB      *sql.DB
}

func (e EmpleadosModel) badRequest(msg string, err error) (entidades.Response, error) {
	return entidades.Response{
		StatusCode: 400,
		Data:       err.Error(),
		Message:    msg,
	}, err
}

func (e EmpleadosModel) goodRequest(msg string, data interface{}) (entidades.Response, error) {
	return entidades.Response{
		StatusCode: 200,
		Data:       data,
		Message:    msg,
	}, nil
}

func (e EmpleadosModel) validarSecuencia(ctx context.Context, nombre string) (int, error) {
	var secuencia int

	stmt, err := e.DB.Prepare(`
	select
		count(secuencia) as secuencia
	from public.empleados
	where LOWER(CONCAT(primer_nombre, REPLACE(primer_apellido, ' ', ''))) like $1;`)
	if err != nil {
		return secuencia, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(nombre).Scan(&secuencia)
	if err != nil {
		return secuencia, err
	}

	return secuencia, nil
}

func (e EmpleadosModel) validarIdentificacion(ctx context.Context, nombre string) (int, error) {
	var total int

	stmt, err := e.DB.Prepare(`
	select 
		count(numero_identificacion) as total
	from public.empleados
	where numero_identificacion = $1;`)
	if err != nil {
		return total, err
	}
	defer stmt.Close()
	err = stmt.QueryRow(nombre).Scan(&total)
	if err != nil {
		return total, err
	}

	return total, nil
}

func (e EmpleadosModel) CrearEmpleado(ctx context.Context, empleado entidades.Empleados) (entidades.Response, error) {

	// validar secuencia
	// validar identificacion no duplicada
	// Aplicar Caso de uso
	model, err := e.CaseUse.CrearEmpleado(ctx, empleado, e.validarIdentificacion, e.validarSecuencia)
	if err != nil {
		return e.badRequest("Error", err)
	}

	tx, err := e.DB.Begin()
	if err != nil {
		return e.badRequest("Error", err)
	}

	{
		stmt, err := tx.Prepare(`SET timezone = 'America/Bogota';`)
		if err != nil {
			tx.Rollback()
			return e.badRequest("Error", err)
		}
		defer stmt.Close()

		if _, err := stmt.Exec(); err != nil {
			tx.Rollback()
			return e.badRequest("Error", err)
		}
	}

	{
		stmt, err := tx.Prepare(`
		INSERT INTO public.empleados(
			secuencia,
			primer_apellido, 
			segundo_apellido, 
			primer_nombre, 
			otros_nombre, 
			pais_empleo, 
			tipo_identificacion, 
			numero_identificacion, 
			correo, 
			fecha_ingreso, 
			area, 
			fecha_registro)
			VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, now());`)
		if err != nil {
			tx.Rollback()
			return e.badRequest("Error", err)
		}
		defer stmt.Close()

		_, err = stmt.Exec(model.Secuencia, model.PrimerApellido,
			model.SegundoApellido, model.PrimerNombre,
			model.OtrosNombre, model.PaisEmpleo.ID,
			model.TipoIdentificacion.ID, model.NumeroIdentificacion,
			model.Correo, model.FechaIngreso,
			model.Area.ID)
		if err != nil {
			tx.Rollback()
			return e.badRequest("Error", err)
		}
	}

	if err := tx.Commit(); err != nil {
		return e.badRequest("Error", err)
	}

	return e.goodRequest("Empleado Creado", "Empleado Creado")
}

func (e EmpleadosModel) BorrarEmpleado(ctx context.Context, id string) (entidades.Response, error) {
	stmt, err := e.DB.PrepareContext(ctx, `DELETE FROM public.empleados WHERE id = $1;`)
	if err != nil {
		return e.badRequest("Error", err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return e.badRequest("Error", err)
	}

	return e.goodRequest("Status Ok", "Empleado Borrado.")
}

func (e EmpleadosModel) oldCampos(ctx context.Context, id string) (string, string, error) {
	var numero_identificacion string
	var nombre string

	stmt, err := e.DB.Prepare(`
	select 
		numero_identificacion,
		LOWER(CONCAT(primer_nombre, REPLACE(primer_apellido, ' ', ''))) as nombre
	from public.empleados
	where id = $1;`)
	if err != nil {
		return numero_identificacion, nombre, err
	}
	defer stmt.Close()
	err = stmt.QueryRow(id).Scan(
		&numero_identificacion,
		&nombre)

	if err != nil {
		return numero_identificacion, nombre, err
	}

	return numero_identificacion, nombre, err
}

// modificarlos todos (excepto la fecha de registro)
// set fecha actualizacion
// modificar correo si cambian los nombres (primer_nombre, primer_apellido)
func (e EmpleadosModel) EditarEmpleado(ctx context.Context, empleado entidades.Empleados) (entidades.Response, error) {

	// validar secuencia
	// validar identificacion no duplicada
	// Aplicar Caso de uso
	// validar identificacion con todos los diferentes a el
	//
	model, err := e.CaseUse.EditarEmpleado(ctx, empleado, e.validarIdentificacion, e.validarSecuencia, e.oldCampos)
	if err != nil {
		return e.badRequest("Error", err)
	}

	stmt, err := e.DB.PrepareContext(ctx, `
	UPDATE public.empleados
		SET 
			secuencia= $1, 
			primer_apellido= $2, 
			segundo_apellido= $3, 
			primer_nombre= $4, 
			otros_nombre= $5, 
			pais_empleo= $6, 
			tipo_identificacion= $7, 
			numero_identificacion= $8,
			correo= $9, 
			fecha_ingreso= $10,
			area= $11, 
			fecha_edicion= $12
	WHERE id = $13;`)
	if err != nil {
		return e.badRequest("Error", err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx,
		model.Secuencia,
		model.PrimerApellido,
		model.SegundoApellido,
		model.PrimerNombre,
		model.OtrosNombre,
		model.PaisEmpleo.ID,
		model.TipoIdentificacion.ID,
		model.NumeroIdentificacion,
		model.Correo,
		model.FechaIngreso,
		model.Area.ID,
		model.FechaEdicion,
		model.ID)
	if err != nil {
		return e.badRequest("Error", err)
	}

	return e.goodRequest("Status Ok", "Empleado Editado.")
}
