INSERT INTO public.areas(nombre)
	VALUES ('Administración'), 
	('Financiera'), 
	('Compras'), 
	('Infraestructura'), 
	('Operación'), 
	('Talento Humano'), 
	('Servicios Varios');

--SELECT id, secuencia, nombre FROM public.areas;
	
	
INSERT INTO public.pais(nombre, dominio)
	VALUES ('Colombia', 'cidenet.com.co'), 
	('Estados Unidos', 'cidenet.com.us');

--SELECT id, secuencia, nombre, dominio FROM public.pais;
	
	
INSERT INTO public.tipo_identificacion(nombre)
	VALUES ('Cédula de Ciudadanía'), 
	('Cédula de Extranjería'), 
	('Pasaporte'), 
	('Permiso Especial');
	
--SELECT id, secuencia, nombre FROM public.tipo_identificacion;