BEGIN;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS public.empleados
(
    id uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    secuencia integer NOT NULL DEFAULT 0,
    primer_apellido character varying(20) NOT NULL,
    segundo_apellido character varying(20) NOT NULL,
    primer_nombre character varying(20) NOT NULL,
    otros_nombre character varying(50) NOT NULL,
    pais_empleo uuid NOT NULL,
    tipo_identificacion uuid NOT NULL,
    numero_identificacion character varying(20) NOT NULL UNIQUE, -- Falta Agregar UNIQUE
    correo character varying(300) NOT NULL UNIQUE,
    fecha_ingreso date NOT NULL DEFAULT CURRENT_DATE,
    area uuid NOT NULL,
    estado boolean NOT NULL DEFAULT true,
    fecha_registro timestamp with time zone NOT NULL DEFAULT CURRENT_DATE,
    fecha_edicion timestamp with time zone,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.pais
(
    id uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    secuencia serial NOT NULL,
    nombre character varying(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.tipo_identificacion
(
    id uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    secuencia serial NOT NULL,
    nombre character varying(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.areas
(
    id uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    secuencia serial NOT NULL,
    nombre character varying(50) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.empleados
    ADD CONSTRAINT "FK_pais_empleados_pais_empleo" FOREIGN KEY (pais_empleo)
    REFERENCES public.pais (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;


ALTER TABLE IF EXISTS public.empleados
    ADD CONSTRAINT "FK_areas_empleados_area" FOREIGN KEY (area)
    REFERENCES public.areas (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;


ALTER TABLE IF EXISTS public.empleados
    ADD CONSTRAINT "FK_tipo_identificacion_empleados_tipo_identificacion" FOREIGN KEY (tipo_identificacion)
    REFERENCES public.tipo_identificacion (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;

END;