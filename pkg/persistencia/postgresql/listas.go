package postgresql

import (
	"context"
	"database/sql"

	"gestion.cidenet.com/pkg/entidades"
)

type ListasModel struct {
	DB *sql.DB
}

func (l ListasModel) badRequest(msg string, err error) (entidades.Response, error) {
	return entidades.Response{
		StatusCode: 400,
		Data:       err.Error(),
		Message:    msg,
	}, err
}

func (l ListasModel) goodRequest(msg string, data interface{}) (entidades.Response, error) {
	return entidades.Response{
		StatusCode: 200,
		Data:       data,
		Message:    msg,
	}, nil
}

func (l ListasModel) ListasPais(ctx context.Context) (entidades.Response, error) {
	var model []entidades.Pais
	rows, err := l.DB.QueryContext(ctx, "SELECT id, secuencia, nombre, dominio FROM public.pais;")
	if err != nil {
		return l.badRequest("Error", err)
	}
	defer rows.Close()

	for rows.Next() {
		item := entidades.Pais{}
		err := rows.Scan(&item.ID,
			&item.Secuencia,
			&item.Nombre,
			&item.Dominio)
		if err != nil {
			return l.badRequest("Error", err)
		}
		model = append(model, item)
	}

	if err := rows.Err(); err != nil {
		return l.badRequest("Error", err)
	}

	return l.goodRequest("Status Ok", model)
}

func (l ListasModel) ListasTipoIdentificacion(ctx context.Context) (entidades.Response, error) {
	var model []entidades.TipoIdentificacion
	rows, err := l.DB.QueryContext(ctx, "SELECT id, secuencia, nombre FROM public.tipo_identificacion;")
	if err != nil {
		return l.badRequest("Error", err)
	}
	defer rows.Close()

	for rows.Next() {
		item := entidades.TipoIdentificacion{}
		err := rows.Scan(&item.ID,
			&item.Secuencia,
			&item.Nombre)
		if err != nil {
			return l.badRequest("Error", err)
		}
		model = append(model, item)
	}

	if err := rows.Err(); err != nil {
		return l.badRequest("Error", err)
	}

	return l.goodRequest("Status Ok", model)
}

func (l ListasModel) ListasAreas(ctx context.Context) (entidades.Response, error) {
	var model []entidades.Areas
	rows, err := l.DB.QueryContext(ctx, "SELECT id, secuencia, nombre FROM public.areas;")
	if err != nil {
		return l.badRequest("Error", err)
	}
	defer rows.Close()

	for rows.Next() {
		item := entidades.Areas{}
		err := rows.Scan(&item.ID,
			&item.Secuencia,
			&item.Nombre)
		if err != nil {
			return l.badRequest("Error", err)
		}
		model = append(model, item)
	}

	if err := rows.Err(); err != nil {
		return l.badRequest("Error", err)
	}

	return l.goodRequest("Status Ok", model)
}

func (l ListasModel) ListarEmpleados(ctx context.Context) (entidades.Response, error) {
	var model []entidades.Empleados
	rows, err := l.DB.QueryContext(ctx, `
	SELECT 
		pe.id, 
		pe.secuencia, 
		pe.primer_apellido, 
		pe.segundo_apellido, 
		pe.primer_nombre, 
		pe.otros_nombre, 
		pp.id as pais_empleo_id,
		pp.nombre as pais_empleo_nombre,
		pp.secuencia as pais_secuencia,
		pp.dominio as pais_dominio,
		pt.id as tipo_identificacion_id,
		pt.nombre as tipo_identificacion_nombre,
		pt.secuencia as tipo_identificacion_secuencia,
		pe.numero_identificacion, 
		pe.correo, 
		COALESCE(to_char(pe.fecha_ingreso, 'DD/MM/YYYY'), ''::text) as fecha_ingreso, 
		pa.id as area_id,
		pa.nombre as area_nombre,
		pa.secuencia as area_secuencia,
		pe.estado, 
		COALESCE(to_char(pe.fecha_registro, 'DD/MM/YYYY HH:mm:ss'), ''::text) as fecha_registro,
		COALESCE(to_char(pe.fecha_edicion, 'DD/MM/YYYY HH:mm:ss'), ''::text) as fecha_edicion
	FROM public.empleados as pe 
	left join public.pais as pp on pp.id = pe.pais_empleo
	left join public.areas as pa ON pa.id = pe.area
	left join public.tipo_identificacion as pt ON pt.id = pe.tipo_identificacion;`)

	if err != nil {
		return l.badRequest("Error", err)
	}
	defer rows.Close()

	for rows.Next() {
		item := entidades.Empleados{}
		err := rows.Scan(
			&item.ID,
			&item.Secuencia,
			&item.PrimerApellido,
			&item.SegundoApellido,
			&item.PrimerNombre,
			&item.OtrosNombre,
			&item.PaisEmpleo.ID,
			&item.PaisEmpleo.Nombre,
			&item.PaisEmpleo.Secuencia,
			&item.PaisEmpleo.Dominio,
			&item.TipoIdentificacion.ID,
			&item.TipoIdentificacion.Nombre,
			&item.TipoIdentificacion.Secuencia,
			&item.NumeroIdentificacion,
			&item.Correo,
			&item.FechaIngreso,
			&item.Area.ID,
			&item.Area.Nombre,
			&item.Area.Secuencia,
			&item.Estado,
			&item.FechaRegistro,
			&item.FechaEdicion)
		if err != nil {
			return l.badRequest("Error", err)
		}
		model = append(model, item)
	}

	if err := rows.Err(); err != nil {
		return l.badRequest("Error", err)
	}

	return l.goodRequest("Status Ok", model)
}

func (l ListasModel) ListarPorIDEmpleado(ctx context.Context, id string) (entidades.Response, error) {
	var model []entidades.Empleados
	stmt, err := l.DB.PrepareContext(ctx, `
	SELECT 
		pe.id, 
		pe.secuencia, 
		pe.primer_apellido, 
		pe.segundo_apellido, 
		pe.primer_nombre, 
		pe.otros_nombre, 
		pp.id as pais_empleo_id,
		pp.nombre as pais_empleo_nombre,
		pp.secuencia as pais_secuencia,
		pp.dominio as pais_dominio,
		pt.id as tipo_identificacion_id,
		pt.nombre as tipo_identificacion_nombre,
		pt.secuencia as tipo_identificacion_secuencia,
		pe.numero_identificacion, 
		pe.correo, 
		COALESCE(to_char(pe.fecha_ingreso, 'DD/MM/YYYY'), ''::text) as fecha_ingreso, 
		pa.id as area_id,
		pa.nombre as area_nombre,
		pa.secuencia as area_secuencia,
		pe.estado, 
		COALESCE(to_char(pe.fecha_registro, 'DD/MM/YYYY HH:mm:ss'), ''::text) as fecha_registro,
		COALESCE(to_char(pe.fecha_edicion, 'DD/MM/YYYY HH:mm:ss'), ''::text) as fecha_edicion
	FROM public.empleados as pe 
	left join public.pais as pp on pp.id = pe.pais_empleo
	left join public.areas as pa ON pa.id = pe.area
	left join public.tipo_identificacion as pt ON pt.id = pe.tipo_identificacion WHERE pe.id = $1;`)
	if err != nil {
		return l.badRequest("Error", err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		return l.badRequest("Error", err)
	}
	defer rows.Close()

	for rows.Next() {
		item := entidades.Empleados{}
		err := rows.Scan(
			&item.ID,
			&item.Secuencia,
			&item.PrimerApellido,
			&item.SegundoApellido,
			&item.PrimerNombre,
			&item.OtrosNombre,
			&item.PaisEmpleo.ID,
			&item.PaisEmpleo.Nombre,
			&item.PaisEmpleo.Secuencia,
			&item.PaisEmpleo.Dominio,
			&item.TipoIdentificacion.ID,
			&item.TipoIdentificacion.Nombre,
			&item.TipoIdentificacion.Secuencia,
			&item.NumeroIdentificacion,
			&item.Correo,
			&item.FechaIngreso,
			&item.Area.ID,
			&item.Area.Nombre,
			&item.Area.Secuencia,
			&item.Estado,
			&item.FechaRegistro,
			&item.FechaEdicion)
		if err != nil {
			return l.badRequest("Error", err)
		}
		model = append(model, item)
	}

	if err := rows.Err(); err != nil {
		return l.badRequest("Error", err)
	}

	return l.goodRequest("Status Ok", model)
}
