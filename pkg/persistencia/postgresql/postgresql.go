package postgresql

import (
	"database/sql"
	"os"

	_ "github.com/lib/pq"
)

// DNS: host=127.0.0.1 user=postgres password=postgres dbname=prueba port=5432 sslmode=disable TimeZone=America/Bogota
func Connection() (*sql.DB, error) {
	dns := os.Getenv("API_DB_DNS")

	db, err := sql.Open("postgres", dns)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
