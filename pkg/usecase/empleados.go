package usecase

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"strings"
	"time"

	"gestion.cidenet.com/pkg/entidades"
)

type EmpleadosCaseUse struct {
	empleado              entidades.Empleados
	validarSecuencia      func(ctx context.Context, nombre string) (int, error)
	validarIdentificacion func(ctx context.Context, nombre string) (int, error)
	ctx                   context.Context
}

/*
El Campo Número de Identificación no se puede repetir.
*/

func (e EmpleadosCaseUse) CrearEmpleado(ctx context.Context, empleado entidades.Empleados, validarIdentificacion, validarSecuencia func(ctx context.Context, nombre string) (int, error)) (entidades.Empleados, error) {

	e.empleado = empleado
	e.validarIdentificacion = validarIdentificacion
	e.validarSecuencia = validarSecuencia
	e.ctx = ctx

	e.cleanValues()
	err := e.validarCamposRequeridos(true)
	if err != nil {
		return e.empleado, err
	}
	err = e.validarRestriccionesDeCaracteres()
	if err != nil {
		return e.empleado, err
	}
	err = e.validarFechaIngreso()
	if err != nil {
		return e.empleado, err
	}
	e.crearCorreo()

	return e.empleado, nil
}

func (e EmpleadosCaseUse) EditarEmpleado(ctx context.Context, empleado entidades.Empleados, validarIdentificacion, validarSecuencia func(ctx context.Context, nombre string) (int, error), oldCampos func(ctx context.Context, id string) (string, string, error)) (entidades.Empleados, error) {

	e.empleado = empleado
	e.validarIdentificacion = validarIdentificacion
	e.validarSecuencia = validarSecuencia
	e.ctx = ctx

	e.cleanValues()
	err := e.validarCamposRequeridos(false)
	if err != nil {
		return e.empleado, err
	}
	err = e.validarRestriccionesDeCaracteres()
	if err != nil {
		return e.empleado, err
	}
	err = e.validarFechaIngreso()
	if err != nil {
		return e.empleado, err
	}
	numero_identificacion, oldname, err := oldCampos(ctx, e.empleado.ID.String())
	if err != nil {
		return e.empleado, err
	}
	if numero_identificacion != e.empleado.NumeroIdentificacion {
		err := e.validarCamposRequeridos(true)
		if err != nil {
			return e.empleado, err
		}
	}

	newName := strings.ToLower(e.empleado.PrimerNombre) +
		strings.ToLower(strings.ReplaceAll(e.empleado.PrimerApellido, " ", ""))

	log.Println(oldname, newName)

	if oldname != newName {
		e.crearCorreo()
	}

	hoy, err := e.timeIn(time.Now())
	if err != nil {
		return e.empleado, err
	}
	e.empleado.FechaEdicion = fmt.Sprintf("%d-%d-%d", hoy.Year(), int(hoy.Month()), hoy.Day()) //, hoy.Hour(), hoy.Minute(), hoy.Second()
	return e.empleado, nil
}

// cleanValues: Limpia los espacios en blanco al principio y al final
func (e *EmpleadosCaseUse) cleanValues() {
	e.empleado.PrimerApellido = strings.TrimSpace(e.empleado.PrimerApellido)
	e.empleado.SegundoApellido = strings.TrimSpace(e.empleado.SegundoApellido)
	e.empleado.PrimerNombre = strings.TrimSpace(e.empleado.PrimerNombre)
	e.empleado.OtrosNombre = strings.TrimSpace(e.empleado.OtrosNombre)
	e.empleado.NumeroIdentificacion = strings.TrimSpace(e.empleado.NumeroIdentificacion)
	e.empleado.Correo = strings.TrimSpace(e.empleado.Correo)
	e.empleado.FechaIngreso = strings.TrimSpace(e.empleado.FechaIngreso)
	e.empleado.FechaRegistro = strings.TrimSpace(e.empleado.FechaRegistro)
	e.empleado.FechaEdicion = strings.TrimSpace(e.empleado.FechaEdicion)
}

// El Campo Fecha Ingreso, No podrá ser superior a la fecha actual, pero sí podrá ser hasta un mes menor.
func (e *EmpleadosCaseUse) validarFechaIngreso() error {
	err := e.ValidarRangoDeFecha()
	if err != nil {
		return err
	}
	return nil
}

// Configuro el time zone a bogota
func (e *EmpleadosCaseUse) timeIn(t time.Time) (time.Time, error) {
	loc, err := time.LoadLocation("America/Bogota")
	if err == nil {
		t = t.In(loc)
	}
	return t, err
}

func (e *EmpleadosCaseUse) validarCamposRequeridos(numeroValid bool) error {

	err := func(value string) error {
		return fmt.Errorf("el campo %s es obligatorio", value)
	}

	isEmpty := func(value string) bool {
		return len(value) == 0
	}

	switch {
	case isEmpty(e.empleado.PrimerApellido):
		return err("Primer Apellido")
	case isEmpty(e.empleado.SegundoApellido):
		return err("Segundo Apellido")
	case isEmpty(e.empleado.PrimerNombre):
		return err("Primer Nombre")
	case isEmpty(e.empleado.NumeroIdentificacion):
		return err("Numero Identificacion")
	case isEmpty(e.empleado.FechaIngreso):
		return err("Fecha Ingreso")
	case isEmpty(e.empleado.PaisEmpleo.ID.String()):
		return err("Pais")
	case isEmpty(e.empleado.TipoIdentificacion.ID.String()):
		return err("Tipo Identificacion")
	case isEmpty(e.empleado.Area.ID.String()):
		return err("Area")
	case isEmpty(e.empleado.PaisEmpleo.Dominio):
		return err("Dominio de Correo")
	default:
		if numeroValid {
			return e.validarNumeroIdent()
		}
		return nil
	}
}
func (e *EmpleadosCaseUse) validarNumeroIdent() error {
	total, er := e.validarIdentificacion(e.ctx, e.empleado.NumeroIdentificacion)
	if er != nil {
		return er
	}
	if total > 0 {
		return fmt.Errorf("ya existe un empleado con la identificacion: %s", e.empleado.NumeroIdentificacion)
	}
	return nil
}

// validarRestriccionesDeCaracteres:
// Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.
// El Campo Otros Nombres: Es opcional y su longitud máxima será de 50 letras.
func (e *EmpleadosCaseUse) validarRestriccionesDeCaracteres() error {
	// Solo letras y espacio sin caracteres especiales
	if err := e.regexValida("^[a-zA-Z ]{1,20}$", e.empleado.PrimerApellido, "Primer Apellido"); err != nil {
		return err
	}
	// Solo letras y espacio sin caracteres especiales
	if err := e.regexValida("^[a-zA-Z ]{1,20}$", e.empleado.SegundoApellido, "Segundo Apellido"); err != nil {
		return err
	}

	// Solo alfanumerico y sin espacios
	if err := e.regexValida("^[a-zA-Z]{1,20}$", e.empleado.PrimerNombre, "Primer Nombre"); err != nil {
		return err
	}

	// Solo alfanumerico con guines y sin espacios
	if len(e.empleado.OtrosNombre) > 0 {
		if err := e.regexValida("^[a-zA-Z ]{1,50}$",
			e.empleado.OtrosNombre, "Otros Nombres"); err != nil {
			return err
		}
	}

	// Solo alfanumerico con guines y sin espacios
	if err := e.regexValida("^[a-zA-Z0-9-]{1,20}$",
		e.empleado.NumeroIdentificacion, "Numero Identificacion"); err != nil {
		return err
	}
	return nil
}

// rgx: regex
// value: campo a evaluar con regex
// campo: nombre del campo
func (e *EmpleadosCaseUse) regexValida(rgx, value, campo string) error {
	r, err := regexp.Compile(rgx)
	if err != nil {
		return err
	}
	if !r.MatchString(value) {
		return fmt.Errorf("el campo %s contiene caracteres no validos o excede el limite de caracteres", campo)
	}
	return nil
}

/*
El Campo Correo electrónico, se debe crear con las siguientes reglas:
Se debe generar automáticamente.
<PRIMER_NOMBRE>.<PRIMER_APELLIDO>.<ID>@<DOMINIO>
El DOMINIO será cidenet.com.co para Colombia y cidenet.com.us para Estados Unidos.
El ID se agrega en caso de estar un usuario registrado con ese email.
*/
func (e *EmpleadosCaseUse) crearCorreo() error {

	email := ""
	emailPrimerNombre := strings.ToLower(e.empleado.PrimerNombre)
	emailPrimerApellido := strings.ToLower(strings.ReplaceAll(e.empleado.PrimerApellido, " ", ""))

	secuencia, err := e.validarSecuencia(e.ctx, emailPrimerNombre+emailPrimerApellido)
	if err != nil {
		return err
	}

	e.empleado.Secuencia = secuencia
	if e.empleado.Secuencia > 0 {
		email = fmt.Sprintf("%s.%s.%d@%s",
			emailPrimerNombre, emailPrimerApellido, e.empleado.Secuencia, e.empleado.PaisEmpleo.Dominio)
	} else {
		email = fmt.Sprintf("%s.%s@%s",
			emailPrimerNombre, emailPrimerApellido, e.empleado.PaisEmpleo.Dominio)
	}
	e.empleado.Correo = email
	return nil
}

func (e *EmpleadosCaseUse) ValidarRangoDeFecha() error {
	layoutISO := "02/01/2006" // DD/MM/YYYY
	fechaIngreso, err := time.Parse(layoutISO, e.empleado.FechaIngreso)
	if err != nil {
		return err
	}

	// cambiar al layout permitido YYYY-MM-DD
	e.empleado.FechaIngreso = fmt.Sprintf("%d-%d-%d", fechaIngreso.Year(), int(fechaIngreso.Month()), fechaIngreso.Day())

	hoy, err := e.timeIn(time.Now())
	if err != nil {
		return err
	}

	// evaluar el anio de la fecha registro
	// 2021 - 2021 = 0; 2021 - 2020 = 1; 2021 - 2019 = 2; 2021 - 2023 = -2; (>=0&&<=1)
	if exp := (hoy.Year() - fechaIngreso.Year()); exp >= 0 && exp <= 1 {
		switch exp {

		// dentro del mismo anio
		case 0:
			// Dia: calcular que el dia del anio no sea mayor al dia actual
			// hoy = 354 (fechaR <= 354)
			if fechaIngreso.YearDay() <= hoy.YearDay() {
				// Mes: hoy  fechaR  si 11 - 11 = 0; 11 - 10 = 1; 11 - 9 = 2; (>=0 && <=1)
				//(11 - 11) = 1 : >= 0 && < 2;
				if dif := int(hoy.Month()) - int(fechaIngreso.Month()); dif >= 0 && dif <= 1 {
					return nil
				}

				return fmt.Errorf("el campo fecha ingreso en meses, no puede ser mayor a un mes del mes actual")

			}
			// el dia de ingreso es mayor al dia actual
			return fmt.Errorf("el campo fecha ingreso en dias, no puede ser mayor a el dia actual")

		// evaluar ingreso de diciembre desde enero
		case 1:
			if int(fechaIngreso.Month()) == 12 {
				return nil
			}
			// el dia de ingreso es mayor al dia actual
			return fmt.Errorf("el campo fecha ingreso en meses, no puede ser mayor a un mes del mes actual")
		}
	} else {
		return fmt.Errorf("el campo fecha ingreso, No podrá ser superior a la fecha actual, pero sí podrá ser hasta un mes menor")
	}

	return nil
}
