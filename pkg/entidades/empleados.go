package entidades

import "github.com/google/uuid"

type Empleados struct {
	ID                   uuid.UUID          `json:"id"`
	Secuencia            int                `json:"secuencia"`
	PrimerApellido       string             `json:"primer_apellido"`
	SegundoApellido      string             `json:"segundo_apellido"`
	PrimerNombre         string             `json:"primer_nombre"`
	OtrosNombre          string             `json:"otros_nombre"`
	PaisEmpleo           Pais               `json:"pais_empleo"`
	TipoIdentificacion   TipoIdentificacion `json:"tipo_identificacion"`
	NumeroIdentificacion string             `json:"numero_identificacion"`
	Correo               string             `json:"correo"`
	FechaIngreso         string             `json:"fecha_ingreso"`
	Area                 Areas              `json:"area"`
	Estado               bool               `json:"estado"`
	FechaRegistro        string             `json:"fecha_registro"`
	FechaEdicion         string             `json:"fecha_edicion"`
}
