package entidades

type Filtros struct {
	PrimerNombre         string `json:"primer_nombre"`
	OtrosNombres         string `json:"otros_nombre"`
	PrimerApellido       string `json:"primer_apellido"`
	SegundoApellido      string `json:"segundo_apellido"`
	TipoIdentificacion   string `json:"tipo_identificacion"`
	NumeroIdentificacion string `json:"numero_identificacion"`
	PaisEmpleo           string `json:"pais_empleo"`
	Correo               string `json:"correo"`
	Estado               bool   `json:"estado"`
}
