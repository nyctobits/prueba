package entidades

import "github.com/google/uuid"

type Pais struct {
	ID        uuid.UUID `json:"id"`
	Secuencia int       `json:"secuencia"`
	Nombre    string    `json:"nombre"`
	Dominio   string    `json:"dominio"`
}
