package entidades

import "github.com/google/uuid"

type TipoIdentificacion struct {
	ID        uuid.UUID `json:"id"`
	Secuencia int       `json:"secuencia"`
	Nombre    string    `json:"nombre"`
}
