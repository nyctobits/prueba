package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (r RoutesAPI) consultarPorID(c echo.Context) error {
	id := c.Param("id")
	resp, err := r.ListasRepository.ListarPorIDEmpleado(c.Request().Context(), id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}
