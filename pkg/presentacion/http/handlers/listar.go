package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (r RoutesAPI) listarPais(c echo.Context) error {
	resp, err := r.ListasRepository.ListasPais(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}

func (r RoutesAPI) listarTipoIdentificacion(c echo.Context) error {
	resp, err := r.ListasRepository.ListasTipoIdentificacion(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}

func (r RoutesAPI) listarAreas(c echo.Context) error {
	resp, err := r.ListasRepository.ListasAreas(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}

func (r RoutesAPI) listarEmpleados(c echo.Context) error {
	resp, err := r.ListasRepository.ListarEmpleados(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}
