package handlers

import (
	"net/http"

	"gestion.cidenet.com/pkg/entidades"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

func (r RoutesAPI) crearEmpleado(c echo.Context) error {
	body := new(entidades.Empleados)
	if err := c.Bind(body); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, entidades.Response{
			Data:       err.Error(),
			Message:    "Error",
			StatusCode: http.StatusBadRequest,
		})
	}

	resp, err := r.EmpleadosRepository.CrearEmpleado(c.Request().Context(), *body)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}

func (r RoutesAPI) editarPorID(c echo.Context) error {
	id := c.Param("id")
	if len(id) == 0 {
		return echo.NewHTTPError(http.StatusBadRequest, entidades.Response{
			Data:       "El ID del empleado no es valido",
			Message:    "Error",
			StatusCode: http.StatusBadRequest,
		})
	}
	body := new(entidades.Empleados)
	if err := c.Bind(body); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, entidades.Response{
			Data:       err.Error(),
			Message:    "Error",
			StatusCode: http.StatusBadRequest,
		})
	}
	body.ID = uuid.MustParse(id)
	resp, err := r.EmpleadosRepository.EditarEmpleado(c.Request().Context(), *body)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}

func (r RoutesAPI) borrarPorID(c echo.Context) error {
	id := c.Param("id")
	resp, err := r.EmpleadosRepository.BorrarEmpleado(c.Request().Context(), id)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, resp)
}
