package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (r RoutesAPI) docs(c echo.Context) error {
	return c.HTML(http.StatusOK, "<strong>Docs!</strong>")
}
