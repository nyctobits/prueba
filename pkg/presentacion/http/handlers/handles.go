package handlers

import (
	"gestion.cidenet.com/pkg/adaptadores/repository"
	"github.com/labstack/echo/v4"
)

type RoutesAPI struct {
	EmpleadosRepository repository.EmpleadosRepository
	ListasRepository    repository.ListasRepository
}

func (r RoutesAPI) InitRoutesV1(e *echo.Echo) {

	base := e.Group("/gestion/api/v1")
	{
		base.GET("/docs", r.docs)
	}

	reg := base.Group("/registro")
	{
		reg.POST("/nuevo", r.crearEmpleado) // posible error de fecha_registro
		reg.PUT("/editar/:id", r.editarPorID)
		reg.DELETE("/borrar/:id", r.borrarPorID)
	}

	con := base.Group("/consulta")
	{
		con.GET("/por_id/:id", r.consultarPorID)
	}

	lista := base.Group("/listas")
	{
		lista.GET("/pais", r.listarPais)
		lista.GET("/tipo_identificacion", r.listarTipoIdentificacion)
		lista.GET("/areas", r.listarAreas)
		lista.GET("/empleados", r.listarEmpleados)
	}

}
