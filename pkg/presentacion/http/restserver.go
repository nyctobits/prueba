package http

import (
	"net"

	"gestion.cidenet.com/pkg/presentacion/http/handlers"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type ServerHttp struct {
	Port   string
	Host   string
	Routes handlers.RoutesAPI
}

func (sh ServerHttp) InitServer() error {
	e := echo.New()

	e.Static("/", "pkg/presentacion/webui")
	//e.Use(middleware.Logger())
	e.Use(middleware.CORS())

	sh.Routes.InitRoutesV1(e)

	if err := e.Start(net.JoinHostPort(sh.Host, sh.Port)); err != nil {
		return err
	}

	return nil
}
