package interfaces

import (
	"context"

	"gestion.cidenet.com/pkg/entidades"
)

type IUseCase interface {
	CrearEmpleado(ctx context.Context, empleado entidades.Empleados, validarIdentificacion, validarSecuencia func(ctx context.Context, nombre string) (int, error)) (entidades.Empleados, error)
	EditarEmpleado(ctx context.Context, empleado entidades.Empleados, validarIdentificacion, validarSecuencia func(ctx context.Context, nombre string) (int, error), oldCampos func(ctx context.Context, id string) (string, string, error)) (entidades.Empleados, error)
}
