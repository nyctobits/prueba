package repository

import (
	"context"

	"gestion.cidenet.com/pkg/entidades"
)

type ListasRepository interface {
	ListasPais(ctx context.Context) (entidades.Response, error)
	ListasTipoIdentificacion(ctx context.Context) (entidades.Response, error)
	ListasAreas(ctx context.Context) (entidades.Response, error)
	ListarEmpleados(ctx context.Context) (entidades.Response, error)
	ListarPorIDEmpleado(ctx context.Context, id string) (entidades.Response, error)
}
