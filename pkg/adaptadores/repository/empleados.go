package repository

import (
	"context"

	"gestion.cidenet.com/pkg/entidades"
)

type EmpleadosRepository interface {
	CrearEmpleado(ctx context.Context, empleado entidades.Empleados) (entidades.Response, error)
	BorrarEmpleado(ctx context.Context, id string) (entidades.Response, error)
	EditarEmpleado(ctx context.Context, empleado entidades.Empleados) (entidades.Response, error)
}
