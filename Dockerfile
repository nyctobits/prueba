FROM golang:1.17.3-alpine3.14
RUN mkdir /app
ADD . /app
WORKDIR /app
## Add this go mod download command to pull in any dependencies
RUN go mod download
## Our project will now successfully build with the necessary go libraries included.
RUN go build -o main cmd/server/main.go
## Our start command which kicks off
## our newly created binary executable
EXPOSE 8180
CMD ["/app/main"]