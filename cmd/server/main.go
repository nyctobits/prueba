package main

import (
	"log"

	"gestion.cidenet.com/pkg/persistencia/postgresql"
	"gestion.cidenet.com/pkg/presentacion/http"
	"gestion.cidenet.com/pkg/presentacion/http/handlers"
	"gestion.cidenet.com/pkg/usecase"
)

func main() {

	db, err := postgresql.Connection()
	if err != nil {
		log.Fatal(err)
	}

	r := handlers.RoutesAPI{
		EmpleadosRepository: postgresql.EmpleadosModel{
			DB:      db,
			CaseUse: usecase.EmpleadosCaseUse{},
		},
		ListasRepository: postgresql.ListasModel{DB: db},
	}

	server := http.ServerHttp{
		Port:   "8180",
		Host:   "",
		Routes: r,
	}

	if err := server.InitServer(); err != nil {
		log.Fatal(err)
	}

}
